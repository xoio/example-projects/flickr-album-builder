
(* setup parameters *)
let argv = Sys.argv
let api = argv.(1)
let user_id = argv.(2)
let num_photos = argv.(3)


(* Encodes photos for writing *)
let encode_photos (p:Ocaml.Flickr.photo) (s:Ocaml.Flickr.photo_size list):Yojson.Basic.t =
  `Assoc[
    ("id",`String p.id);
    ("title", `String p.title);
    ("sizes", `List(List.map (fun (size:Ocaml.Flickr.photo_size) -> `Assoc[

     ("width", `Int size.width);
     ("height", `Int size.height);
     ("label", `String size.label);
     ("url", `String size.url)
     ]) s))

  ]

(* Construct the final Assoc object for writing *)
let build_list photos =
  `Assoc[
     ("photos", `List(photos))
   ]

let () =
  (* Build the list of photos *)
  let photos = Ocaml.Flickr.photo_url api user_id num_photos
  |> Ocaml.Flickr.get_data
  |> Lwt_main.run
  |> Ocaml.Flickr.extract_photos
  |> List.mapi(fun idx (p:Ocaml.Flickr.photo) ->
    print_endline("Encoding photo " ^ string_of_int(idx));
    (* Extract sizes, then encode photos into list *)
    let sizes = Ocaml.Flickr.size_url api p.id num_photos
    |> Ocaml.Flickr.get_data
    |> Lwt_main.run
    |> Ocaml.Flickr.extract_sizes in
    encode_photos p sizes;

  ) in

  (* Finally final prep for photos then write to file *) 
  let list = build_list photos in
  let _ = Yojson.Basic.to_file "test.json" list in
  print_endline("Done")
