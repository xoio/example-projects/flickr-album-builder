open Yojson.Basic.Util
open Cohttp_lwt_unix
open Lwt
open Cohttp



(* describes the photo size including 
    - width 
    - height 
    - url *)
type photo_size = {
  width : int ;
  height : int;
  label:string;
  url : string ;
}

(* Type for the parsed data. Should hold 
    - id 
    - list of image urls 
    - title *)
type photo = {
  id : string;
  title : string;
}


(* Extracts photos from the JSON response from Flickr and extracts the key elements *)
let extract_photos res =
  Yojson.Basic.from_string res
  |> member "photos"
  |> member "photo"
  |> to_list
  |> List.map(fun p -> 
    match p with 
    | `Assoc assoc -> {
      id = (List.assoc "id" assoc ) |> to_string;
      title = (List.assoc "title" assoc) |> to_string;
      } 
    | _ -> failwith "Unable to parse content into list of Photos"
  )

(* Writes data to file *)
let write_json filename data =
  Yojson.Basic.to_file filename data


(* extracts the size information from the json of a size url request *)
let extract_sizes res =
  Yojson.Basic.from_string res
  |> member "sizes"
  |> member "size"
  |> to_list
  |> List.map(fun p ->
    match p with
    | `Assoc assoc -> {
       width = (List.assoc "width" assoc) |> to_int;
       height = (List.assoc "height" assoc) |> to_int;
       url = (List.assoc "source" assoc) |> to_string;
       label = (List.assoc "label" assoc) |> to_string;
     }
    | _ -> failwith "Issue parsing size data"

  )


(* build url to grab photos *)
let photo_url api user_id per_page =
  "https://flickr.com/services/rest/?" ^
  "method=flickr.people.getPublicPhotos" ^
  "&api_key=" ^ api ^
  "&user_id=" ^ user_id ^
  "&format=json&nojsoncallback=1" ^
  "&per_page=" ^ per_page


(* build url to grab photo sizes *)
let size_url api photo_id per_page=
  "https://flickr.com/services/rest/?" ^
  "method=flickr.photos.getSizes" ^
  "&api_key=" ^ api ^
  "&photo_id="^photo_id^
  "&format=json&nojsoncallback=1" ^
  "&per_page=" ^ per_page



(* Returns the api response from Flickr **)
let rec get_data url =
  Client.get (Uri.of_string url) >>= fun (resp, body) ->
  let code = resp |> Response.status |> Code.code_of_status in
  (*Printf.printf "Code was %d \n" code;*)
  
  if code == 200 then body |> Cohttp_lwt.Body.to_string >|= fun body ->
  body

  (* Note that this only handles one redirect which "should" be enough *)
  else  if code == 302 then
    let headers = Cohttp.Response.headers resp in
    let location = Cohttp.Header.get headers "location" in
    match location with
    | None -> failwith "Unable to redirect"
    | Some url ->  get_data url

  else failwith "Unable to retrieve json"

